module Letscode
#This file contains all of the read/write file operations

    #reads the yaml file
    def read_file
      parsed = begin
        # YAML.load(File.open(File.expand_path("../config.yaml", __FILE__)))
        YAML.load(File.open(File.expand_path("../userConfig.yaml", __FILE__))).inspect
      rescue ArgumentError => e
        puts "Could not parse YAML: #{e.message}"
      end
    end

    #writes to yaml file for user stored projects
    #we will turn data into an array of key values
    #and maybe change the yaml file we write to.
    def write_file(values)
      File.open(File.expand_path("../letscode/userConfig.yaml"), "w") {|f| f.write(values.to_yaml)}
    end

    def list_projects
        parsed = begin
            # YAML.load(File.open(File.expand_path("../config.yaml", __FILE__)))
            YAML.load(File.open(File.expand_path("../userConfig.yaml", __FILE__)))
        rescue ArgumentError => e
            puts "Could not parse YAML: #{e.message}"
        end
    end

end