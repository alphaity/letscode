module Letscode

    def open_sublime(directory)
      #the gsub is needed to help whitespace issues with file directories
      sublime_command = "/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl".gsub(/ /, '\ ') #This is the mac directory
    #  %x[] is the command to invoke shell commands.
      %x[ #{sublime_command} #{directory.gsub(/ /, '\ ')} ]
    end

    def open_vscode(directory)
      vscode_command = 'open -a "Visual Studio Code"'
      %x[ #{vscode_command} #{directory.gsub(/ /, '\ ')} ]
    end

    def start_mongo
      %[mongod]
    end

end