module Letscode

attr_accessor :project

    require 'yaml'
    require 'watir-webdriver'

    def get_project(project)
      #gets the project details for the project we pass in and puts it in @project - an instance variable
      raise "Project does not exist or has not been configured. Please run letscode config." unless read_file[project]; return @project = read_file[project]
    end

    #gets the keys from the saved hash and processes them.
    #then directs them to their respective methods
    def process_keys
      @project.each do | key, value|
        case key
          when "sublime"
            open_sublime value
          when "vscode"
            open_vscode value
          when "mongo"
            start_mongo if value.eql? true
         else
          goto_website value
       end
      end
    end


    def watir_launch
      #the :chrome tag tells Watir to load a new chrome instance \
      #the default browser is firefox
      @browser = Watir::Browser.new #:chrome
    end

    def goto_website(url)
      #add tab supoort here
      watir_launch
      @browser.goto url
    end

    def get_config
      #Here will ask questions and get all the valuable input needed  for each project
      #STDIN is needed due to the passing of args in the console command
      puts "What is the name of your project?"
      name = STDIN.gets.chomp
      puts "Are you using sublime or vscode? Type 'sublime' or 'vscode'"
      editor = STDIN.gets.chomp
      puts "What is the url path to the project?"
      path = STDIN.gets.chomp
      puts "What is the name of the website you want to go to? Trello, github, etc"
      site = STDIN.gets.chomp
      puts "What is the url of #{site}?"
      url = STDIN.gets.chomp
      puts "NAME: #{name} - EDITOR: #{editor} - PATH: #{path} - SITE: #{site} - URL: #{url}"
      #take values and put in hash
      project = [name => [[ editor => "#{path}" ], [ site => "http://#{url}"]]]
    end
end