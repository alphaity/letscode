require 'thor'

module Letscode
    class CLI < Thor
        include Thor::Actions
        include Letscode

        #main method
        desc "start [PROJECT]", "To start a project, type 'letscode start [PROJECT]'"
        def start(project)
          get_project(project)
          process_keys
        end

        desc "config", "To config a project, please type 'letscode config'"
        def config
            config = get_config
            write_file config
        end

        desc "list", "Shows all projects in the config file."
        def list
           puts  "#{list_projects.to_yaml}"
        end

        desc "delete [PROJECT]", " To delete a file type 'letscode delete [PROJECT].'"
        def delete(project)
            config = read_file
            config.delete project
            write_file config
        end
    end
end