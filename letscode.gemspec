# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'letscode/version'

Gem::Specification.new do |spec|
  spec.name          = "letscode"
  spec.version       = Letscode::VERSION
  spec.authors       = ['Nick Stalter', 'Darrell Pappa']
  spec.email         = ['hey@couchsurfers.io']

  spec.summary       = %q{Fetches all of your project related programs and web pages.}
  spec.description   = %q{Fetches all of your project related programs and web pages. These include, Sublime Text, Github/Gitlab, Trello, etc.}
  spec.homepage      = 'https://gitlab.com/couchsurfers.io/letscode'
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = ["letscode"]
  spec.require_paths = ["lib"]

#  spec.extra_rdoc_files = ['README.md', 'ChangeLog.md']

  spec.add_dependency "watir-webdriver", "~> 0"
  spec.add_dependency "thor"
  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
end
